const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const users = require('./routes/users');

app.use(bodyParser.json());

app.use('/users', users);

app.listen(3000, () =>{
    console.log('Connected to server port 3000...');
});