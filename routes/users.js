const express = require('express');
const router = express.Router();
const model = require('../models/index');

//select all
router.get('/', async (req, res) =>{
    try
    {
        const users = await model.users.findAll({});
        res.send(users);
    }catch(err)
    {
        res.json({message : err});
    }
});

//insert 
router.post('/', async (req,res) =>{
    try{
        const users = await model.users.create({
            name : req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            address: req.body.address,
            gender: req.body.gender
        });
        if(users) return res.status(200).json({
            'status':'OK', 
            'messages': 'User berhasil ditambah',
            'data': req.body,
        });
    }catch(err)
    {
        res.status(404).json({messages:'error'});
    }
});

//update
router.put('/:usersID', async (req,res)=>{
    try{
        const users = await model.users.update({
            name : req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            address: req.body.address,
            gender: req.body.gender
        },
        {
            where: {
                id: req.params.usersID
            }
        });
        //console.log(users);
        if(users != 0) return res.status(200).json({
            'status': 'OK',
            'messages': 'Data berhasil diupdate',
            'data': req.body,
        });
    }catch(err)
    {
        res.status(404).json({messages:'error'});
    }
});

//delete
router.delete('/:usersID', async (req,res) =>{
    try{
        const users = model.users.destroy({
            where:{
                id: req.params.usersID
            }
        });
        return res.status(200).json({
            'status': 'OK',
            'messages': 'Data berhasil didelete',
            'data': req.body,
        });
    }catch(err)
    {
        res.status(404).json({messages:'error'});
    }
});

module.exports = router;